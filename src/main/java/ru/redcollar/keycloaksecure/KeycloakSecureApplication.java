package ru.redcollar.keycloaksecure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KeycloakSecureApplication {

    public static void main(String[] args) {
        SpringApplication.run(KeycloakSecureApplication.class, args);
    }

}
