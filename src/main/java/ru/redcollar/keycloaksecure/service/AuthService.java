package ru.redcollar.keycloaksecure.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.redcollar.keycloaksecure.dto.KeycloakToken;
import ru.redcollar.keycloaksecure.dto.UserDto;

@Service
@Slf4j
@RequiredArgsConstructor
public class AuthService {

    private final KeycloakService keycloakService;

    public KeycloakToken getToken(UserDto user) {
        KeycloakToken token = keycloakService.getTokenByCredentials(user);
        log.info("access_token {}", token);
        return token;
    }

    public KeycloakToken refreshToken(KeycloakToken token) {
        KeycloakToken refreshedToken = keycloakService.getTokenByRefreshToken(token);
        log.info("refreshed token {}", token);
        return refreshedToken;
    }
}
