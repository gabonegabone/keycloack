package ru.redcollar.keycloaksecure.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import ru.redcollar.keycloaksecure.enums.OrderStatus;

@Service
@RequiredArgsConstructor
@Slf4j
public class MessageSenderService {

    private final static String ROUTING_KEY_STATUS = "purchase.update.status";

    private final TopicExchange topicExchange;

    private final RabbitTemplate rabbitTemplate;

    public void sendStatusUpd(OrderStatus status) {
        rabbitTemplate.convertAndSend(topicExchange.getName(), ROUTING_KEY_STATUS, status);
        log.info("[x] message \"{}\" sent!", status);
    }
}
