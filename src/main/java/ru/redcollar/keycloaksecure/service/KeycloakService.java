package ru.redcollar.keycloaksecure.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import ru.redcollar.keycloaksecure.dto.KeycloakToken;
import ru.redcollar.keycloaksecure.dto.UserDto;

@Service
@Slf4j
@RequiredArgsConstructor
public class KeycloakService {

    private static final String GRANT_TYPE_PASSWORD = "password";
    private static final String GRANT_TYPE_REFRESH_TOKEN = "refresh_token";
    private static final String KEYCLOAK_TOKEN_URI = "%s/realms/%s/protocol/openid-connect/token";

    @Value("${keycloak.auth-server-url}")
    private String authServerUrl;

    @Value("${keycloak.realm}")
    private String realm;

    @Value("${keycloak.resource}")
    private String resource;

    private final RestTemplate restTemplate;

    public KeycloakToken getTokenByCredentials(UserDto user) {
        MultiValueMap<String, String> urlParams = new LinkedMultiValueMap<>();
        urlParams.add("client_id", resource);
        urlParams.add("grant_type", GRANT_TYPE_PASSWORD);
        urlParams.add("username", user.getUsername());
        urlParams.add("password", user.getPassword());

        return getToken(urlParams);
    }

    public KeycloakToken getTokenByRefreshToken(KeycloakToken token) {
        MultiValueMap<String, String> urlParams = new LinkedMultiValueMap<>();
        urlParams.add("client_id", resource);
        urlParams.add("grant_type", GRANT_TYPE_REFRESH_TOKEN);
        urlParams.add("refresh_token", token.getRefreshToken());

        return getToken(urlParams);
    }

    private KeycloakToken getToken(MultiValueMap<String, String> urlParams) {
        String uri = String.format(KEYCLOAK_TOKEN_URI, authServerUrl, realm);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(urlParams, headers);

        log.info("sending request to Keycloak server for token...");
        AccessToken keycloakToken = restTemplate.postForObject(uri, request, AccessToken.class);
        if (keycloakToken == null) {
            String msg = "KeycloakService: getToken: null object has been returned";
            log.info(msg);
            throw new NullPointerException(msg);
        }
        String accessToken = (String) keycloakToken.getOtherClaims().get("access_token");
        String refreshToken = (String) keycloakToken.getOtherClaims().get("refresh_token");
        KeycloakToken token = new KeycloakToken(accessToken, refreshToken);
        log.info("keycloakToken:" + token);
        return token;
    }
}
