package ru.redcollar.keycloaksecure.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.redcollar.keycloaksecure.dto.KeycloakToken;
import ru.redcollar.keycloaksecure.dto.UserDto;
import ru.redcollar.keycloaksecure.service.AuthService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
public class AuthenticationController {

    private final AuthService authService;

    @PostMapping("/token")
    public ResponseEntity<KeycloakToken> getToken(@RequestBody UserDto userDto) {
        KeycloakToken token = authService.getToken(userDto);
        return ResponseEntity.ok(token);
    }

    @PostMapping("/refresh")
    public ResponseEntity<KeycloakToken> refreshToken(@RequestBody KeycloakToken token) {
        KeycloakToken refreshedToken = authService.refreshToken(token);
        return ResponseEntity.ok(refreshedToken);
    }
}
