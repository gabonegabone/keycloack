package ru.redcollar.keycloaksecure.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.redcollar.keycloaksecure.enums.OrderStatus;
import ru.redcollar.keycloaksecure.service.MessageSenderService;

import javax.annotation.security.RolesAllowed;

@RestController
@RequiredArgsConstructor
@RequestMapping("/msg")
public class MessageSendingController {

    private final MessageSenderService messageSenderService;

    @RolesAllowed("ROLE_ADMIN")
    @PostMapping("/upd-status")
    public ResponseEntity<HttpStatus> updateOrderStatus(@RequestParam("status") OrderStatus orderStatus) {
        messageSenderService.sendStatusUpd(orderStatus);
        return ResponseEntity.ok().build();
    }
}
