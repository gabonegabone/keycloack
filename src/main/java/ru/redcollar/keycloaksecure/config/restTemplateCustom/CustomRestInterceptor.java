package ru.redcollar.keycloaksecure.config.restTemplateCustom;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

@Slf4j
public class CustomRestInterceptor implements ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException {
        log.info("uri: {}",request.getURI());
        log.info("headers: {}",request.getHeaders());
        log.info("http method: {}",request.getMethod());
        return execution.execute(request, body);
    }
}
