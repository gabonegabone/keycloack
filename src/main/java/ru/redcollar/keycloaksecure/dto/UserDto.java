package ru.redcollar.keycloaksecure.dto;

import lombok.Data;

@Data
public class UserDto {

    private String username;
    private String password;
}
