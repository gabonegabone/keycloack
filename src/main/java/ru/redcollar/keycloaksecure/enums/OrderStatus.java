package ru.redcollar.keycloaksecure.enums;

public enum OrderStatus {
    DELIVERING,
    PROCESSING,
    DENIED,
    COMPLETED
}
